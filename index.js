// Exponent Operator (3) **3

let getCube = 5**3;


// Template Literals (4) ``

console.log(`The cube of 5 is ${getCube}.`)

// Array Destructuring (5) ``
const address = ["258", "Washington Ave NW", "California", "90011"];
let [stno, street, state, zipcode] = address;
console.log(`I live at ${stno}, ${street}, ${state}, ${zipcode}`)


// Object Destructuring (7)
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

let {name, species, weight, measurement} = animal;

console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`);

// Arrow Functions (8)
let numbers = [1, 2, 3, 4, 5]; 

	const numArr = numbers.forEach((num) => console.log(num))



// Javascript Classes (11)

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
let dog1 = new Dog("rascal", 2, "aspin");
let dog2 = new Dog("batdog", 3, "mongrel");

console.log(dog1);
console.log(dog2);
